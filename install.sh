#!/bin/bash

# Check if the script is running as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

# Update package list and install dependencies
apt install build-essential libpcap-dev libpcre3-dev libnet1-dev zlib1g-dev luajit hwloc libdnet-dev libdumbnet-dev bison flex liblzma-dev openssl libssl-dev pkg-config libhwloc-dev cmake cpputest libsqlite3-dev uuid-dev libcmocka-dev libnetfilter-queue-dev libmnl-dev autotools-dev libluajit-5.1-dev libunwind-dev libfl-dev -y

# Clone and install libdaq
git clone https://github.com/snort3/libdaq.git
cd libdaq
./bootstrap
./configure
make
make install

cd ../

# Download and install gperftools
wget https://github.com/gperftools/gperftools/releases/download/gperftools-2.9.1/gperftools-2.9.1.tar.gz
tar -xvf gperftools-2.9.1.tar.gz
cd gperftools-2.9.1
./configure
make
make install

cd ../

# Clone and build snort3
git clone https://github.com/snort3/snort3
cd snort3
./configure_cmake.sh --prefix=/usr/local --enable-tcmalloc
cd build
make
make install

export LD_LIBRARY_PATH=/usr/local/lib
snort -V

