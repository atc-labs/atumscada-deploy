-- snort.lua
-- Enable JSON logging
alert_json = {
    fields = [[ action class b64_data dir dst_addr 
                dst_ap dst_port eth_dst eth_len eth_src 
                eth_type gid icmp_code icmp_id icmp_seq 
                icmp_type iface ip_id ip_len msg mpls 
                pkt_gen pkt_len pkt_num priority proto 
                rev rule_service sid src_addr src_ap 
                src_port tcp_ack tcp_flags tcp_len tcp_seq 
                tcp_win tos ttl udp_len vlan ]],
    file = true,
    limit = 1000
}

-- Additional configuration settings
-- ... (other settings)
